# T-Mobile exercise

## Exercise

Prepare the python script that consume the data from Yahoo money API use pandas to get the currency data for EUR (last 30 days), calculate Moving Average, Standard deviation from SMA to draw graph for currency, Bollinger Bands

## Plan

1. Get data from API
2. Use pandas to load data into dataframe
3. Calculate
   1. Moving average
   2. Standard deviation from SMA
   3. Calculate bollinger bands upper and lower

## Running notebook

1. Create venv or not
   ```bash
   virtualenv -p . venv
   ```
2. (Optional) Start venv
   ```bash
   source venv/bin/activate
   ```

3. Install required packages
   ```
   pip install -r requirements.txt
   ```